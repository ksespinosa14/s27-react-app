import React from 'react';

// create a Context Object
const UserContext = React.createContext();

export const UserProvider = UserContext.Provider;

export default UserContext;