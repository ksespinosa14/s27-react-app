import React from 'react';
// import Jumbotron from 'react-bootstrap/Jumbotron';
// import Button from 'react-bootstrap/Button';
import { Jumbotron, Row, Col } from 'react-bootstrap';
import PropTypes from "prop-types";
import { Link } from 'react-router-dom';

export default function Banner({dataProp}) {
    const { title, content, destination, label } = dataProp;

    return (
        <Row>
            <Col>
                <Jumbotron>
                    <h1>{title}</h1>
                    <p>{content}</p>
                    <Link to={destination}>{label}</Link>
                </Jumbotron>
            </Col>
        </Row>
    )
}

Banner.propTypes = {
    // shape() is used to check that a prop object conforms to a specified shape
    dataProp: PropTypes.shape({
        title: PropTypes.string.isRequired,
        content: PropTypes.string.isRequired,
        destination: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired
    })
}