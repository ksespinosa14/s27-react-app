import React, { useState, useEffect } from 'react';
import PropTypes from "prop-types";
import { Card, Button } from 'react-bootstrap';

export default function Course({ courseProp: { name, description, price, start_date, end_date } }) {
    // const { name, description, price, start_date, end_date } = courseProp;

    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(10);
    //let count = 0;
    const [isOpen, setIsOpen] = useState(true);

    function enroll() {
        setCount(count + 1);
        setSeats(seats - 1);
        // ++count;
        console.log('Enrollees: ' + count);
        console.log('Available seats: ' + seats);
    }
    /*
    use effec(()=>{
        code block
    }, [states to trigger useEffect])
    */
    useEffect(()=>{
        if(seats === 0){
            setIsOpen(false);
        }
    }, [seats]);

    return (
        <Card className="course">
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Text as="div">
                    <h3>Decription:</h3>
                    <p>{description}</p>
                    <h4>Price:</h4>
                    <p>PHP {price}</p>
                    <p>Start Date: {start_date}</p>
                    <p>End Date: {end_date}</p>
                </Card.Text>
                    {isOpen
                    ?
                    <Button className="bg-primary" onClick={enroll}>Enroll</Button>
                    :
                    <Button className="bg-danger" disabled onClick={enroll}>Not Available</Button>
                    }
            </Card.Body>
        </Card>
    )

}
// checks if the Course component is getting the correct prop type/ data structure
Course.propTypes = {
    // shape() is used to check that a prop object conforms to a specified shape
    courseProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}