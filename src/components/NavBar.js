import React, { useContext } from 'react';
import Navbar from 'react-bootstrap/NavBar';
import Nav from 'react-bootstrap/Nav';
import UserContext from '../UserContext';

export default function NavBar() {

    const { user } = useContext(UserContext);

    return (
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="/">React-Bootstrap</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">
                    <Nav.Link href="/">Home</Nav.Link>
                    <Nav.Link href="/courses">Courses</Nav.Link>
                    {(user.isAdmin)
                    ?
                    <Nav.Link href="/addcourse">Create Course</Nav.Link>
                    :
                    null
                    }
                    {(user.email !== null)
                        ?
                        <Nav.Link href="/logout">Logout</Nav.Link>
                        :
                        <React.Fragment>
                            <Nav.Link href="/login">Login</Nav.Link>
                            <Nav.Link href="/register">Register</Nav.Link>
                        </React.Fragment>
                    }
                </Nav>
            </Navbar.Collapse>
        </Navbar>
    )
}