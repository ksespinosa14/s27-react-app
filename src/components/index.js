import Banner from './Banner';
import Highlights from './Highlights';
import Course from './Course';

export {
    Banner,
    Highlights,
    Course
}