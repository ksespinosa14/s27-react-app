import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';


export default function AddCourse() {
    const [id, setId] = useState('');
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [status, setStatus] = useState('');
    const [startDate, setStartDate] = useState('');
    const [endDate, setEndDate] = useState('');

    function submit(e) {
        e.preventDefault();
        console.log(name + " with ID: " + id + " is set to start on " + startDate + " for PHP " + price + "\n" + status + "\n" + endDate + "\n" + description);
        
        //clear input fields after submission
		setId('');
		setName('');
		setDescription('');
		setPrice('');
		setStatus('');
		setStartDate('');
		setEndDate('');
    }


    return (
    <Form onSubmit={e => submit(e)}>
        <Form.Group controlId="id">
            <Form.Label>Course ID</Form.Label>
            <Form.Control
                type="text"
                placeholder="ID"
                value={id}
                onChange={(e) => setId(e.target.value)}
                required
            />
        </Form.Group>

        <Form.Group controlId="name">
            <Form.Label>Course Name</Form.Label>
            <Form.Control
                type="text"
                placeholder="Name"
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
            />
        </Form.Group>

        <Form.Group controlId="description">
            <Form.Label>Course Description</Form.Label>
            <Form.Control
                type="text"
                placeholder="Description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
            />
        </Form.Group>

        <Form.Group controlId="price">
            <Form.Label>Course Price</Form.Label>
            <Form.Control
                type="text"
                placeholder="Price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                required
            />
        </Form.Group>

        <Form.Group controlId="onOffer">
            <Form.Label>Status</Form.Label>
            <Form.Control
                type="text"
                placeholder="True or False"
                value={status}
                onChange={(e) => setStatus(e.target.value)}
                required
            />
        </Form.Group>

        <Form.Group controlId="start_date">
            <Form.Label>Start Date</Form.Label>
            <Form.Control
                type="date"
                placeholder="Start Date"
                value={startDate}
                onChange={(e) => setStartDate(e.target.value)}
                required
            />
        </Form.Group>

        <Form.Group controlId="end_date">
            <Form.Label>End Date</Form.Label>
            <Form.Control
                type="date"
                placeholder="End Date"
                value={endDate}
                onChange={(e) => setEndDate(e.target.value)}
                required
            />
        </Form.Group>
        
        <Button className="bg-primary" type="submit">
            Create Course
		</Button>

    </Form>
    )
}