import React, { useContext } from "react";
import { Button, Table } from 'react-bootstrap';
import Course from "../components/Course";
import UserContext from "../UserContext";
import coursesData from "../data/courses";

export default function Courses() {
    const { user } = useContext(UserContext);

    console.log(coursesData);

    const courses = coursesData.map(indivCourse => {
        if(indivCourse.onOffer){
            return (
                <Course key={indivCourse.id} courseProp={indivCourse} />
            )
        } else {
            return null;
        }

    })

    const coursesRows = coursesData.map(indivCourse =>{
        return (
            <tr key={indivCourse.id}>
                <td>{indivCourse.id}</td>
                <td>{indivCourse.name}</td>
                <td>{indivCourse.price}</td>
                <td>{indivCourse.onOffer ? 'Open' : 'Closed'}</td>
                <td>{indivCourse.start_date}</td>
                <td>{indivCourse.end_date}</td>
                <td>
                    <Button className="bg-warning">Update</Button>
                    <Button className="bg-danger">Disable</Button>
                </td>
            </tr>
        )
    })

    return (
        user.isAdmin === true
            ?
            <React.Fragment>
                <h1>Course Dashboard</h1>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Status</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {coursesRows}
                    </tbody>
                </Table>
            </React.Fragment>
            :
            <React.Fragment>
                {courses}
            </React.Fragment>
    )
}