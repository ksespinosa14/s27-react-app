import React from 'react';
import { Banner } from "../components";

export default function Error() {
    const data = {
        title: "404 - Not found",
        content: "The page you are looking for cannot be found",
        destination: "/",
        label: "Back home"
    }

    return (
        <React.Fragment>
            <Banner dataProp={data} />
        </React.Fragment>
    )
}