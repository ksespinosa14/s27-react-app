import React from 'react';
import { Container } from 'react-bootstrap';
// import Banner from '../components/Banner';
// import Highlights from '../components/Highlights';
// import Course from '../components/Course';
import { Banner, Highlights } from "../components";

export default function Home() {

    const data = {
        title: "Zuitt Coding Bootcamp",
        content: "Opportunities for everyone, everywhere.",
        destination: "/courses",
        label: "Enroll Now"
    }

    return (
        <React.Fragment>
            <Container>
                <Banner dataProp={data} />
                <Highlights />
            </Container>
        </React.Fragment>
    )
}