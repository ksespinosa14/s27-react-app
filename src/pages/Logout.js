import React, { useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import UserContext from '../UserContext';

export default function Logout(){
    const { unsetUser, setUser } = useContext(UserContext);
    
    // Clear localStorage
    useEffect(()=>{
        unsetUser(); // same to -> localStorage.clear();
    })

    return (
        <Redirect to='/login'/>
    )
}